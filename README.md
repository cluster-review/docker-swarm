# Docker-Swarm-Vagrant

This a project which could be used it as playground to learn and experiment with [Docker Swarm](https://docs.docker.com/engine/swarm/), especially for **on-premise** installations.

> The code is just for fun and learning, do not use it for production.
> Minimum 4 GB of free RAM, 1GB for each node.

## Prerequisites
- Vagrant
- Virtualbox
- Ansible

## Virtual Infrastructure

[Vagrant](https://www.vagrantup.com/) with [Virtualbox](https://www.virtualbox.org/) as provider will be used to create the infrastructure. One server will be the Docker Swarm manager and two servers as Docker Swarm workers. The virtual machines will run Ubuntu 20.04 (focal).

Start up the infrastructure:

```bash
vagrant up
```

## Docker Swarm Installation

Ansible roles to setup the cluster:

* docker: It will install docker on all the servers.
* docker-swarm-manager: It will initialize Docker Swarm manager and produce the variables for the other nodes to join the cluster.
* docker-swarm-worker: It will help the nodes to join the cluster.
* docker-registry: It will install docker registry on the manager host.

### Installation
```bash
ansible-playbook docker-swarm.yml
```

#### List the nodes

You should now be able to list the docker nodes:

```bash
ansible manager1 -b -m shell -a 'docker node ls'
```

```
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
kgp50yziko2tskz7s17bv1f8s *   m01        Ready     Active         Leader           20.10.7
v1t2y8ynhhtut1kyhhimbk36m     w01        Ready     Active                          20.10.7
xejryjgq462p88h22l43ufiq9     w02        Ready     Active                          20.10.7
qdov04y0ec5kvn7qzoijaf9xf     w03        Ready     Active                          20.10.7
```

#### Optional visualizer

Add [Swarm Visualizer](https://github.com/dockersamples/docker-swarm-visualizer):

```bash
ansible manager1 -b -m shell -a 'docker run -it -d -p 8080:8080 -v /var/run/docker.sock:/var/run/docker.sock dockersamples/visualizer'
```

Navigate your browser to: http://192.168.33.11:8080


## Deploying Applications

### Deploy 2 replicas of Nginx and expose them

Build the special nginx image on the manager1 and push it to its docker registry:
```bash
ansible manager1 -b -m shell -a 'cd /vagrant/apps/nginx && docker build . -t localhost:5000/mynginx && docker push localhost:5000/mynginx'
```

Create a service with our special nginx:
```bash
ansible manager1 -b -m shell -a 'docker service create --replicas 3 -p 8081:80 --name mynginx 192.168.33.11:5000/mynginx:latest'
```


### Visit the Applications

You can now access _any_ node in the docker swarm on the `published` port (8081) to access the application.

```bash
for i in 11 21 22 23; do echo -n "192.168.33.$i: "; curl -s http://192.168.33.$i:8081; done;
```

You should see something like this:
```
192.168.33.11: Sunday, 27-Jun-2021 13:24:23 UTC: 4ddea050ffda
192.168.33.21: Sunday, 27-Jun-2021 13:24:23 UTC: 397d0650421d
192.168.33.22: Sunday, 27-Jun-2021 13:24:23 UTC: 4ddea050ffda
192.168.33.23: Sunday, 27-Jun-2021 13:24:23 UTC: e245b0858c05
```

## Other Docker swarm commands

Scaling:

```bash
ansible manager1 -b -m shell -a 'docker service scale mynginx=3'
```

Removing service:

```bash
ansible manager1 -b -m shell -a 'docker service rm mynginx'
```

Drain a node:

```bash
ansible manager1 -b -m shell -a 'docker node update --availability drain w01'
```

Activate a node (after drain):

```bash
ansible manager1 -b -m shell -a 'docker node update --availability active w01'
```

## Destroy
When done playing around, to shutdown and remove the virtual machines completely run:

```bash
vagrant destroy
```
